require("dotenv").config();
const express = require("express");
const app = express();
const PORT = process.env.PORT || 3000;
require("./database")();

const controller = require("./app/controller");

app.use("/", controller);

app.listen(PORT, function () {
  console.log("Server started and listening at port", PORT);
});
