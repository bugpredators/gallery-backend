var AWS = require("aws-sdk");

var s3 = new AWS.S3();
const DEFAULT_BUCKET_NAME = "photo-gallery-demo";
const DEFAULT_EXPIRES = 120;

async function getUploadSignedUrl(params) {
  const {
    bucket,
    fileName = new Date().getTime(),
    mimeType,
    expires = DEFAULT_EXPIRES,
  } = params;
  return await s3.getSignedUrl("putObject", {
    Bucket: DEFAULT_BUCKET_NAME || bucket,
    Key: fileName,
    Expires: expires,
    ContentType: mimeType,
  });
}

async function getDownloadSignedUrl(params) {
  const { bucket, fileName, expires = DEFAULT_EXPIRES } = params;
  const s3 = new AWS.S3();
  return await s3.getSignedUrl("getObject", {
    Bucket: DEFAULT_BUCKET_NAME || bucket,
    Key: fileName,
    Expires: expires,
  });
}

module.exports = { getUploadSignedUrl, getDownloadSignedUrl };
