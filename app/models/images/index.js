const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

const ImageSchema = new Schema({
  S3Url: {
    type: String,
    required: true,
  },
  categoryId: {
    type: Mongoose.Types.ObjectId,
    ref: "Category",
    required: true,
  },
  name: String,
  description: String,
});

module.exports = ImageSchema;
