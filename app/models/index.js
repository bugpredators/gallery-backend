const Mongoose = require("mongoose");

const Category = require("./category");
const Image = require("./images");

module.exports = {
  Category: Mongoose.model("Category", Category),
  Image: Mongoose.model("Image", Image),
};
