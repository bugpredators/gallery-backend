const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

const categorySchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  previewImgURI: {
    type: String,
    required: true,
  },
});

module.exports = categorySchema;
