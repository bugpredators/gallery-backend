const express = require("express");
const router = express.Router();

const categoryService = require("../../services/category");
const imageService = require("../../services/image");

async function getImageById(req, res) {
  const id = req.params.id;
  return res.json({ msg: `Fetching Image with id ${id}` });
}

async function getImageByCategory(req, res) {
  const { categoryId } = req.params;
  const images = await imageService.getImagesByCategory({ categoryId });
  return res.json({ data: images });
}

async function insertImageInCategory(req, res) {
  const { categoryId, s3ImageUrl, name, description } = req.body;
  const category = await categoryService.getCategories({ categoryId });
  if (!category) return res.status(401).json({ msg: "Category not found" });
  const image = await imageService.insertImageInCategory({
    categoryId,
    s3Url: s3ImageUrl,
    name,
    description,
  });
  return res
    .status(201)
    .json({ msg: "Image inserted successfully", data: image });
}

router.get("/:id", getImageById);

router.get("/:categoryId", getImageByCategory);

router.post("/", insertImageInCategory);

module.exports = router;
