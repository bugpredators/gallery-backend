const express = require("express");
const router = express.Router();

const categoryService = require("../../services/category");

async function getCategories(req, res) {
  const categories = await categoryService.getCategories();
  return res.json({ data: categories });
}

router.get("/", getCategories);

module.exports = router;
