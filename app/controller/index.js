const express = require("express");
const router = express.Router();

const fileRouter = require("./file");
const categoryRouter = require("./categories");
const imageRouter = require("./image");

router.use("/file", fileRouter);
router.use("/category", categoryRouter);
router.use("/image", imageRouter);

module.exports = router;
