const express = require("express");
const router = express.Router();

const s3Helper = require("../../utils/s3Helper");

async function getUploadPreSignedURI(req, res) {
  const { type } = req.params; //enum ["upload", "download"]
  const { mimeType } = req.body;
  if (type === "upload") {
    const presignedURI = await s3Helper.getUploadSignedUrl({ mimeType });
    return res.json({ data: { presignedURI } });
  }
  return res.status(400).json({ msg: `Invalid type "${type}"` });
}

router.post("/url/:type", getUploadPreSignedURI);

module.exports = router;
