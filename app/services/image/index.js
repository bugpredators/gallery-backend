const Mongoose = require("mongoose");
const Models = require("../../models");

async function getImageById(params) {
  const { _id } = params;
  return await Models.Image.find({
    _id: Mongoose.Types.ObjectId(_id),
  }).lean();
}

async function getImagesByCategory(params) {
  const { categoryId } = params;
  return await Models.Image.find({ categoryId }).lean();
}

async function insertImageInCategory(params) {
  return await Models.Image.insert(params);
}

module.exports = {
  getImageById,
  getImagesByCategory,
  insertImageInCategory,
};
