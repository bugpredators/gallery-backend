const Mongoose = require("mongoose");
const Models = require("../../models");

async function getCategoryById(params) {
  const { _id } = params;
  return await Models.Category.find({
    _id: Mongoose.Types.ObjectId(_id),
  }).lean();
}

async function getCategories() {
  return await Models.Category.find().lean();
}

module.exports = {
  getCategoryById,
  getCategories,
};
